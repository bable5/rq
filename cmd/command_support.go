package cmd

import (
	"os"

	"github.com/prometheus/common/log"
	"gitlab.com/bable5/rq/requests"
)

func addNewRequest(file string, args []string) error {
	request, err := buildRequestFromArgs(args)
	if err != nil {
		return err
	}

	log.Info(file)
	return requests.WriteRequest(file, request)
}

func runSavedRequest(file string) error {
	request, err := requests.ReadRequest(file)

	if err != nil {
		return err
	}

	return runRequest(request)
}

func runAdHocRequest(args []string) error {
	request, err := buildRequestFromArgs(args)
	if err != nil {
		return err
	}

	return runRequest(request)
}

func runRequest(request *requests.Request) error {
	expandEnvironmentVariables(request)
	log.Debug(request)
	return requests.DefaultExecRequest(request)
}

func expandEnvironmentVariables(request *requests.Request) {
	request.Verb = os.ExpandEnv(request.Verb)
	request.URL = os.ExpandEnv(request.URL)
}

func buildRequestFromArgs(args []string) (*requests.Request, error) {
	verb := args[0]
	url := args[1]

	return requests.NewRequest("", verb, url)
}
