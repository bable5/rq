/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"errors"

	"github.com/prometheus/common/log"
	"github.com/spf13/cobra"
)

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run a request",
	Long:  `Run a save request or an ad-hoc.`,
	Run: func(cmd *cobra.Command, args []string) {
		var err error
		if filename != "" {
			err = runSavedRequest(filename)
		} else {
			if len(args) != 2 {
				err = errors.New("must have an http verb and url")
			}
			err = runAdHocRequest(args)
		}

		if err == nil {
			log.Error(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(runCmd)
	addFileFlag(runCmd, false)
}
