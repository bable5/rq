package requests

import (
	uuid "github.com/satori/go.uuid"
)

type ReqeustID = string

type Request struct {
	ID      ReqeustID              `json:"id"`
	Name    string                 `json:"name"`
	Verb    string                 `json:"httpVerb"`
	URL     string                 `json:"url"`
	Body    map[string]interface{} `json:"body"`
	Headers map[string][]string    `json:"headers"`
}

func NewRequest(name, verb, url string) (*Request, error) {
	requestUUID, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}
	requestID := requestUUID.String()

	// body := make(map[string]interface{})

	return &Request{
		ID:   requestID,
		Name: name,
		Verb: verb,
		URL:  url,
	}, nil
}
