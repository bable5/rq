package requests

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
)

func WriteRequest(path string, request *Request) error {
	err := ensureDirExists(path)

	if err != nil {
		return err
	}

	b, err := json.MarshalIndent(request, "", "  ")

	if err != nil {
		return err
	}

	return ioutil.WriteFile(path, b, 0644)
}

func ensureDirExists(path string) error {
	base := filepath.Dir(path)

	if _, err := os.Stat(base); os.IsNotExist(err) {
		return os.MkdirAll(base, os.ModeDir|os.ModePerm)
	}

	return nil
}

func ReadRequest(path string) (*Request, error) {
	jsonFile, err := os.Open(path)
	defer jsonFile.Close()

	if err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var request Request

	json.Unmarshal(byteValue, &request)

	return &request, nil
}
