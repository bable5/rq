package requests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type ResponseProcessor func(resp *http.Response, err error) error

func DefaultResponseProcessor(resp *http.Response, err error) error {
	if err != nil {
		return err
	}

	body, err := ioutil.ReadAll(resp.Body)

	bodyString := string(body)
	fmt.Println(resp.Status)
	fmt.Println("------------------------------------------------------------------------------")
	for name, headers := range resp.Header {
		name = strings.ToLower(name)
		fmt.Println(name, strings.Join(headers, ","))
	}
	fmt.Println("------------------------------------------------------------------------------")
	fmt.Println(bodyString)

	return err
}

func DefaultExecRequest(request *Request) error {
	return ExecRequest(request, DefaultResponseProcessor)
}

func ExecRequest(reqTemplate *Request, responseProcessor ResponseProcessor) error {
	client := &http.Client{}

	req, err := createRequest(reqTemplate)

	addHeaders(reqTemplate, req)

	if err != nil {
		return err
	}

	resp, err := client.Do(req)

	if resp != nil {
		defer resp.Body.Close()
	}

	return responseProcessor(resp, err)
}

func createRequest(request *Request) (*http.Request, error) {
	if request.Body != nil {
		buf := new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode(request.Body)
		if err != nil {
			return nil, err
		}
		log.Println("Sending request with body")
		return http.NewRequest(request.Verb, request.URL, buf)
	} else {
		log.Println("Sending request without body")
		return http.NewRequest(request.Verb, request.URL, nil)
	}
}

func addHeaders(reqTemplate *Request, req *http.Request) {
	if reqTemplate.Headers != nil {
		for name, headers := range reqTemplate.Headers {
			for _, header := range headers {
				req.Header.Add(name, header)
			}
		}
	}
}
